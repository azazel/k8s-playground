# -*- coding: utf-8 -*-
# :Project:   kubernetes-playground --
# :Created:   mar 24 apr 2018 19:44:28 CEST
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2018 Alberto Berti
#

export SHELL := /bin/bash
NIX := $(shell which nix || echo "$(HOME)/.nix-profile/bin/nix")

$(NIX):
	@echo "Installing nix..."
	@curl https://nixos.org/nix/install | sh
