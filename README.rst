.. -*- coding: utf-8 -*-
.. :Project:   kubernetes-playground --
.. :Created:   mar 24 apr 2018 18:55:41 CEST
.. :Author:    Alberto Berti <alberto@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2018 Alberto Berti
..

=======================
 Kubernetes playground
=======================

This is a litte experiment of setting up a `Kubernetes`_ playground. It
uses two main elements: `nix`_ and `minicube`_.

.. _kubernetes: https://kubernetes.io/
.. _nix: https://nixos.org/nix/
.. _minicube: https://github.com/kubernetes/minikube


Kubernetes
==========

Architecture
------------

At a very high level, Kubernetes has the following main components:

- one or more master nodes
- one or more worker nodes
- distributed key-value store, like **etcd**.

.. figure:: assets/k8s-architecture.png

Kubernetes Configuration
------------------------

Kubernetes can be installed using different configurations. The four
major installation types are briefly presented below:

- **All-in-One Single-Node Installation**

  With all-in-one, all the master and worker components are installed
  on a single node. This is very useful for learning, development, and
  testing. This type should not be used in production. Minikube is one
  such example, and we are going to explore it in future chapters.

- **Single-Node etcd, Single-Master, and Multi-Worker Installation**

  In this setup, we have a single master node, which also runs a
  single-node etcd instance. Multiple worker nodes are connected to
  the master node.

- **Single-Node etcd, Multi-Master, and Multi-Worker Installation**

  In this setup, we have multiple master nodes, which work in an HA
  mode, but we have a single-node etcd instance. Multiple worker nodes
  are connected to the master nodes.

- **Multi-Node etcd, Multi-Master, and Multi-Worker Installation**

  In this mode, etcd is configured in a clustered mode, outside the
  Kubernetes cluster, and the nodes connect to it. The master nodes
  are all configured in an HA mode, connecting to multiple worker
  nodes. This is the most advanced and recommended production setup.


Nix
===

`Nix`_ is a package manger that works without the need of super user
permissions (apart from the installation process) with `many nifty
features`__  and works on both Linux and macOS. I will use it to
install the other required pieces of the puzzle.

__ https://nixos.org/nix/about.html

While most of the times it installs pre-built binaries, it's made so
the source/binary distribution is transparently managed.
To install it, just execute::

  $ make

in this directory.

The pre-requirements are:

- curl
- bzip2


kubectl
=======

kubectl is a binaryused to access any Kubernetes cluster.

Minikube
========

In most of the cases, Minikube runs inside a VM on Linux, Mac, or
Windows. Therefore, we need to make sure that we have the supported
hardware and the hypervisor to create VMs. Next, we outline the
requirements to run Minikube on our workstation/laptop:

- On Linux: VirtualBox or KVM hypervisors

  ..note::

    Minikube also supports a --vm-driver=none option that runs the
    Kubernetes components on the host and not in a VM. Docker is
    required to use this driver, but no hypervisor. If you
    use --vm-driver=none, be sure to specify a bridge network for
    Docker. Otherwise, it might change between network restarts,
    causing loss of connectivity to your cluster.

- On macOS: Hyperkit driver, xhyve driver, VirtualBox or VMware Fusion
  hypervisors


- On Windows: VirtualBox or Hyper-V hypervisors VT-x/AMD-v
  virtualization must be enabled in BIOS Internet connection on first
  run.
