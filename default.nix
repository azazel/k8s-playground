with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "k8s-playground";
  buildInputs = [
    kubectl
    minikube
  ];
}
